extern crate gtk;
extern crate gdk;
extern crate gdk_pixbuf;
extern crate rustc_serialize;

mod constants;

#[derive(RustcDecodable)]
struct Program {
    label: String,
    executable: String
}

impl ToJson for Program {
    fn to_json(&self) -> Json {
        let mut d = BTreeMap::new();
        d.insert("label".to_string(), self.label.to_json());
        d.insert("executable".to_string(), self.executable.to_json());
        Json::Object(d)
    }
}

struct Register {
    wallpaper: String,
    top_bar_programs: Vec<Program>
}


impl Register {
    fn get_wallpaper(&self) -> &str {
        //glupi hax nie wiem dlaczego sie robi cudzyslow na poczatku i na koncu napisu
        &self.wallpaper[1..self.wallpaper.len() - 1]
    }
}

use std::fs::File;
use std::io::prelude::*;

use std::process::Command;

use gtk::prelude::*;
use gtk::{Window, WindowType, Image, Layout, MenuItem, MenuBar, FileChooserDialog, FileChooserAction, 
    ResponseType};

use gdk::{Screen};

use gdk_pixbuf::Pixbuf;

use std::collections::BTreeMap;
use rustc_serialize::json::{self, Json, ToJson};

fn read_configuration(file_path: &str) -> Register {

    let mut file = File::open(file_path).expect("Could not find configuration file");
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).expect("Cant put string into buffer");

    let data = Json::from_str(&buffer).unwrap();

    let obj = data.as_object().unwrap();

    let wallpaper = obj.get("wallpaper").unwrap().to_string();

    let mut top_bar_programs: Vec<Program> = Vec::new();

    let programs_json = obj.get("top_bar_programs").unwrap();
     
    for program_json in programs_json.as_array().unwrap() {

        let program: Program = json::decode(&program_json.to_string()).unwrap();
        top_bar_programs.push(program);
    }

    Register{ wallpaper: wallpaper, top_bar_programs: top_bar_programs }
}

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    let screen = Screen::get_default().unwrap(); 
    let window = Window::new(WindowType::Toplevel);
    window.set_title("Background");
    window.set_decorated(false);
    window.set_default_size(screen.get_width(), screen.get_height());

    let register = read_configuration("./conf/rust-weston-compositor.json");

    let menu_bar = MenuBar::new();
    let change_background = MenuItem::new_with_label("Zmiana tapety");

    for program in &register.top_bar_programs {

        let menu_item = MenuItem::new_with_label(program.label.as_ref());
        menu_bar.append(&menu_item);

        let copy = program.executable.clone();
        menu_item.connect_button_press_event( move |_, _| {
            let _ = Command::new("sh")
                .arg("-c")
                .arg(&copy).spawn()
                .unwrap_or_else(|e| {
                    println!("Error spawning child: {}", e); panic!("spawning child")});

            Inhibit(false)
        });

    }

    menu_bar.append(&change_background);

    let layout = Layout::new(None, None);

    let background_image = Pixbuf::new_from_file_at_scale(register.get_wallpaper(),
                                                          screen.get_width(), screen.get_height(), true).expect("Background image was not found");

    let background = Image::new_from_pixbuf(Some(&background_image));

    layout.add(&menu_bar);
    layout.put(&background, 0, constants::TOP_BAR_HEIGHT);

    let file_window = Window::new(WindowType::Toplevel);

    change_background.connect_button_press_event( move |_, _| {

        let dialog = FileChooserDialog::new(Some("Choose a file"), Some(&file_window),
        FileChooserAction::Open);
        dialog.add_buttons(&[
                           ("Open", ResponseType::Ok.into()),
                           ("Cancel", ResponseType::Cancel.into())
        ]);

        if dialog.run() == gtk::ResponseType::Ok.into() {
            let filename = dialog.get_filename().unwrap();

            let background_image = Pixbuf::new_from_file_at_scale(filename.to_str().unwrap(), screen.get_width(),
            screen.get_height(), true).unwrap();
            background.set_from_pixbuf(Some(&background_image));
        }
        dialog.destroy();

        Inhibit(false)
    });

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    window.add(&layout);
    window.show_all();

    gtk::main();
}

