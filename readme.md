# Kompozytor
Jest to prosty kompozytor stworzony na potrzeby pracy magisterskiej.
## Przygotowanie i kompilacja
Pierwsze co będzie potrzebne do kompilacji to język programowania Rust. Najprościej jest go zainstalować, używając narzędzia rustup. 

```bash
curl https://sh.rustup.rs -sSf | sh
```

Możliwe, że będzie potrzebne dodanie cargo do zmiennej środowiskowej PATH, więcej można dowiedzieć się [tu](https://www.rust-lang.org/pl-PL/install.html).

Program był uruchamiany na fedorze, która już ma wiele bibliotek związanym z waylandem, jeśli program będzie kompilowany na czymś innym na pewno będą potrzebne biblioteki:
```bash
wlc-devel, wlc, libwayland-server, libwayland-server-devel, libwayland-client, libwayland-client-devel 
```

Następnie będzie potrzeba doinstalować biblioteki developerskie:
```bash
pam-devel, mtdev-devel,mesa-libgbm-devel,mesa-libGLES-devel ,gtk3-devel,gdk-pixbuf2-xlib ,pango-devel,atk-devel,gdk-pixbuf2-devel,cairo-devel,dbus-devel ,mesa-libEGL-devel,mesa-libwayland-client-devel
```
Potem trzeba wejść na katalog z projektem i go skompilować poleceniem
```bash
cargo build
```

Jeśli będzie brakowało nam jeszcze jakichś bibliotek kompilator powinien dać znać o tym.

## Uruchamianie

Kompozytor powinno się uruchamiać z wiersza poleceń używając komendy

```bash
cargo run --bin rust-weston-compositor
```
